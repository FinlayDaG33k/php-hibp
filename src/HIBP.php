<?php
  namespace FinlayDaG33k\HIBP;

  use Cake\Http\Client;

  class HIBP {
    private const HIBP_HOST = "https://api.pwnedpasswords.com/";

    public function checkPassword(string $hash): array {
      // Request dataset from HIBP
      $resp = (new Client())
        ->get('https://api.pwnedpasswords.com/range/' . $hash, [
          'timeout' => 1
        ]);

      // Turn our response into an array
      $hashes = explode("\r\n", $resp->getStringBody());
      
      // Return our dataset
      return $hashes;
    }
  }